Thank you for taking the time to view my assessment.
If you wish to set the project up to view, this readme will outline all needed steps.
Before running NPM install, I would suggest going to the Resources/js/components/index.jsx file
and updating the values for the API calls. I use valet and so the URLs represent where I have the application running on my machine.
Once that is done, run composer install and npm install. 
There is no special .env file needed so you should be able to copy the env example file.
Then php artisan migrate --seed as I did provide a seeder for the drinks table.
Finally, run npm run dev or npm run watch and that should have everything going.
On the demo site at http://34.205.18.161/mjfreeway/public/, I have already created a user. However,
I did not do so in the seeder. So, you may need to create a user. In using the demo, feel free to create new users, etc.
Though the requirements called for creating the application for one user, I felt it would be of better use in evaluating my skills if I added the ability to have many users.
That is why I created this in the way I did with the ability to create users and see the particular users caffeine intake.
Again, thank you for your time in viewing my assessment. I hope to hear from you soon.