<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intake extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function drink()
    {
        return $this->belongsTo(Drinks::class);
    }
}
