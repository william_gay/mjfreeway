<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;
use App\Drinks;
use App\Http\Resources\DrinksResource as DrinksResource;

class DrinksController extends Controller
{
    public function index()
    {
        $drinks = Drinks::all();
        return DrinksResource::collection($drinks);
    }

}
