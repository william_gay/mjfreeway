<?php

namespace App\Http\Controllers;


use App\Http\Resources\IntakeResource as IntakeResource;
use App\Intake;
use Carbon\Carbon;
use Illuminate\Http\Request;


class IntakeController extends Controller
{

    public function index(Request $request)
    {
        $today = Carbon::now()->format('Y-m-d');
        $id = $request->id;
        $res = Intake::with('drink')
            ->where('user_id', '=', $id)
            ->whereDate('date', '=', $today)
            ->get();

        return IntakeResource::collection($res);
    }

    public function storeDrink(Request $request)
    {
        $newDrink = $request->drink;
        $intake = new Intake();
        $intake->user_id = $request->userId;
        $intake->drink_id = $request->id;
        $intake->caffeine_amt = $newDrink['caffeineAmt'];
        $intake->date = Carbon::now();
        $intake->save();
    }
}
