<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drinks extends Model
{
    public function intakes()
    {
        return $this->hasMany(Intake::class);
    }
}
