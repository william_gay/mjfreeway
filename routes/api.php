<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::options(
    '/{any:.*}',
    [
        'middleware' => ['CorsMiddleware'],
        function () {
            return response(['status' => 'success']);
        }
    ]
);

Route::middleware('auth:api')->get(
    '/user',
    function (Request $request) {
        return $request->user();
    }
);

Route::post(
    '/auth/user',
    function () {
        return response()->json(['user' => auth()->user()]);
    }
)->middleware('auth');

Route::get('drinks', 'DrinksController@index');
Route::get('consumed/{id}', 'IntakeController@index');
Route::post('addConsumed/{id}', 'IntakeController@storeDrink');
