@extends('layouts.app')

@section('content')
    <div class="container">
        <section id="rendered"></section>
    </div>
@endsection
<input type="hidden" name="name" value="{{ \Illuminate\Support\Facades\Auth::user()->id}}"/>
<script>
    let userId = '{{ \Illuminate\Support\Facades\Auth::user()->id}}';
</script>
