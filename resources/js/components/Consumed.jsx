import React, {Component} from 'react'

export default class Consumed extends Component {
    constructor(props) {
        super();
        this.state = {
            consumed: []
        };
        this.consumed = this.props;
    }

    render() {
        let body = [];
        if (this.props.consumed) {
            this.props.consumed.forEach((row, idx) => {
                    body.push
                    (
                        <div key={idx} className="row">
                            <div className="col-md-3">
                                {row.drink}
                            </div>
                            <div className="col-md-9">
                                {row.caffeineAmt} mg caffeine
                            </div>
                        </div>
                    )
                }
            );
        }

        return <div className="card">
            <div>{body}</div>
        </div>
    }
}
