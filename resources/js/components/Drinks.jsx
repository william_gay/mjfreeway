import React, {Component} from 'react'

export default class Drinks extends Component {
    constructor(props) {
        super();
        this.state = {
            drinks: []
        };
        this.drinks = this.props;
    }

    render() {
        let body = [];
        if (this.props.drinks) {
            this.props.drinks.forEach((row, idx) => {
                    body.push
                    (
                        <div key={idx} className="row">
                            <div className="col-md-3">
                                {row.name}
                            </div>
                            <div className="col-md-9">
                                {row.description}
                            </div>
                        </div>
                    )
                }
            );
        }

        return <div className="card">
            <div>{body}</div>
        </div>
    }
}


