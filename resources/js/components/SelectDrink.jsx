import React, {Component} from 'react'

export default class SelectDrink extends Component {
    constructor(props) {
        super();
        this.state = {
            drinks: [],
            consumed: [],
            selectedOption: "Select A Drink"
        };
        this.drinks = this.props;
    }

    render() {
        if (this.props.drinks) {
            const {selectedOption} = this.state;
            const value = selectedOption && selectedOption.value;
            return (
                <div className="form-group">
                    <select
                        id='selectDrink'
                        name='selectDrink'
                        onChange={this.props.confirmDrink}
                        defaultValue={'DEFAULT'}
                        value={this.props.selectedDrink}>
                        <option value="DEFAULT" disabled>Select a Drink ...</option>
                        {this.props.drinks.map(option => {
                            return (
                                <option key={option.id} value={option.id}>
                                    {option.name}
                                </option>
                            );
                        })};
                    </select>
                    <br/>
                    <button className="btn btn-primary" style={{marginTop: '20px'}} onClick={this.props.addDrink}>
                        Confirm Selected Drink
                    </button>
                </div>
            )

        }
        return null
    }
}