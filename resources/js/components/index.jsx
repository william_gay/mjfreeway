import React, {Component} from 'react'
import axios from 'axios';
import Drinks from './Drinks';
import Consumed from './Consumed';
import SelectDrink from "./SelectDrink";

class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
            drinks: null,
            consumed: null,
            totalCaffeine: 0,
            prevDrinks: null,
            curDrink: null,
        };
        this.confirmDrink = this.confirmDrink.bind(this);
        this.addDrink = this.addDrink.bind(this);
    }

    componentDidMount() {
        axios.get('http://reactlaravel.test/api/drinks')
            .then(res => {
                this.setState({drinks: res.data.data});
            });

        axios.get('http://reactlaravel.test/api/consumed/' + userId)
            .then(res => {
                for (let ndx in res.data.data) {
                    this.state.totalCaffeine += res.data.data[ndx].caffeineAmt;
                }
                this.setState(({consumed: res.data.data, userId: userId}))
            })
    }

    confirmDrink(e) {
        this.setState(
            prevState => {
                return {
                    prevDrinks: prevState.drinks
                };
            },
        );
        this.state.curDrink = parseInt(e.target.value, 10);
    }

    addDrink() {
        let newDrink = {};
        for (let ndx = 0; ndx < this.state.drinks.length; ndx++) {
            if (this.state.drinks[ndx].id === this.state.curDrink) {
                newDrink.caffeineAmt = this.state.drinks[ndx].caffeine;
                newDrink.drink = this.state.drinks[ndx].name;
                this.state.totalCaffeine += parseInt(this.state.drinks[ndx].caffeine, 10);
            }

        }
        this.setState(prevState => ({
            consumed: [...prevState.consumed, newDrink]
        }));
        axios.post('http://reactlaravel.test/api/addConsumed/' + this.state.curDrink, {
            drink: newDrink,
            userId: this.state.userId
        })
            .then(res => {
                console.log(res);
                console.log(res.data)
            })
    }

    render() {
        return (
            <div>
                <h2>Available Drinks</h2>
                < Drinks
                    drinks={this.state.drinks}
                />
                <h2>Today's Consumption</h2>
                <Consumed
                    consumed={this.state.consumed}

                />
                <h3>Currently, you have consumed {this.state.totalCaffeine} mg today.</h3>
                <hr/>
                <h3>What drink would you like?</h3>
                <SelectDrink
                    drinks={this.state.drinks}
                    consumed={this.state.consumed}
                    confirmDrink={this.confirmDrink}
                    addDrink={this.addDrink}
                    placeholder={"Select Drink"}
                    selectedDrink={this.state.selectedDrink}
                />
            </div>
        )
    }

}

export default Index